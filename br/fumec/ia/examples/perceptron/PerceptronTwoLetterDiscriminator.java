/**
 * 
 */
package br.fumec.ia.examples.perceptron;

import br.fumec.ia.rna.Perceptron;


/**
 * Test of the perceptron class
 * 
 * @author  Alair Dias Júnior
 * @version 1.0
 */
public class PerceptronTwoLetterDiscriminator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Perceptron p = new Perceptron(9);
		
		double inputs[][] = {{1,1,1,0,1,0,0,1,0}, // Real T
							{1,0,1,1,1,1,1,0,1}}; // Real H
		double output[] = {1,  // Output 1.0 means T
						  -1}; // Output -1.0 means H
		
		double test[][] = {{1,0,1,0,1,0,0,1,0},  // Distorted T
						   {1,0,1,1,0,1,1,0,1}}; // Distorted H
		
		try{
			p.train(inputs, output, 0.2, 0, 1000);
			
			if (p.process(test[0]) == 1.0) System.out.println("Distorted T recognized.");
			else  System.out.println("Distorted T NOT recognized.");
			
			if (p.process(test[1]) == -1.0) System.out.println("Distorted H recognized.");
			else  System.out.println("Distorted H NOT recognized.");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
