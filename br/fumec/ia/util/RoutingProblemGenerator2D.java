/**
 * 
 */
package br.fumec.ia.util;

import java.awt.geom.*;
import java.io.*;
import java.util.*; 

/**
 * Generates an Instance of the Routing Problem
 * Ensures that the graph is connected
 * 
 * @author  Alair Dias Júnior
 * @version 1.0
 */
public class RoutingProblemGenerator2D {

	private Point2D[] m_vertices;
	private Double[][] m_distance;
	
	/**
	 * Used to sort the points by euclidean distance.
	 * Only the nearest Neighbors are connected. 
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 */
	private class PointDistance implements Comparable<PointDistance>
	{
		private int m_point;
		private Double m_distance;
		
		/**
		 * Class Constructor 
		 * 
		 * @author  Alair Dias Júnior
		 * @version 1.0
		 * @param point The point of origin
		 * @param distance The distance to the point being calculated 
		 */
		public PointDistance(int point, Double distance)
		{
			m_point = point;
			m_distance = distance;
		}
		/**
		 * Returns the number of the point of  
		 * 
		 * @author  Alair Dias Júnior
		 * @version 1.0
		 * @return The point of origin 
		 */
		public int getPoint(){return m_point;}
		
		/**
		 * Returns the distance from the point of origin to the point being calculate.
		 * 
		 * @author  Alair Dias Júnior
		 * @version 1.0
		 * @return the distance from the point of origin to the point being calculate
		 */
		public double getDistance(){return m_distance.doubleValue();}
		
		public int compareTo(PointDistance anotherPoint) {  
		    return Double.compare(getDistance(), ((PointDistance)anotherPoint).getDistance()); 
		 }
	}
	
	/**
	 * Creates a random Double ranging from min to max
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param min The minimum value of the Double
	 * @param max The maximum value of the Double
	 * @return The random Double Value
	 */
	private Double randomCoord(double min, double max)
	{
		
		Double value = Math.random() * (max - min) + min;
		return value;
	}
	
	/**
	 * Generates a collection of random points in a Plane 
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param num The number of points to be generated
	 * @param xMin The minimum value for the point x coordinate
	 * @param xMax The maximum value for the point x coordinate
	 * @param yMin The minimum value for the point y coordinate
	 * @param yMax The maximum value for the point y coordinate
	 */
	public void genVertices(int num, double xMin, double xMax, double yMin, double yMax)
	{
		m_vertices = new Point2D[num];
		for (int i = 0; i < num; ++i)
		{
			m_vertices[i] = new Point2D.Double(randomCoord(xMin, xMax),
											randomCoord(yMin, yMax));
		}
	}
	
	
	/**
	 * Calculates the Straight Line distance for the given points
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param num The number of points to be generated
	 * @param p1 The first point
	 * @param p2 The second point
	 */
	private double straightLineDistance(int p1, int p2)
	{
		return m_vertices[p1].distance(m_vertices[p2]);
	}
	
	/**
	 * Creates a list of points sorted ascending by the distance to the given point
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param p The point of origin
	 * @return A list of points sorted ascending by the distance to the point of origin
	 */
	ArrayList<PointDistance> nearestNeighbors(int p)
	{
		ArrayList<PointDistance> nearest = new ArrayList<PointDistance>();
		
		
		for (int i = 0; i < m_vertices.length; ++i)
			if (i != p) nearest.add(new PointDistance(i,straightLineDistance(i,p)));
		
		Collections.sort(nearest);
		
		return nearest;
	}
	
	/**
	 * Connect the points of the graph. This method ensures the graph is connected.
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param chance The chance of two neighbors being connected
	 * @param percentInc The percentage of increment of the sld.
	 */
	public void connect(double chance, double percentInc)
	{
		ConnectivityProblem cp = new ConnectivityProblem(m_vertices.length);
		while (!cp.connected())
		{
			cp.reset();
			m_distance = new Double[m_vertices.length][m_vertices.length];
			
			for (int i = 0; i < m_vertices.length; ++i)
				for (int j = i; j < m_vertices.length; ++j)
				{
					if (i!=j){
						m_distance[i][j]= -1.0;
						m_distance[j][i]= -1.0;
					}else{
						m_distance[i][j]= 0.0;
					}
				}
			
			for (int i = 0; i < m_vertices.length; ++i)
			{
				ArrayList<PointDistance> nearest = nearestNeighbors(i);
				for (int j = 0; j < 5; ++j)
				{
					int q = nearest.get(j).getPoint();
					if (cp.connect(i, q))
					{
						double sld = straightLineDistance(i,q);
						m_distance[i][q] = sld + Math.random()*percentInc*sld;
						m_distance[q][i] = m_distance[i][q];
					}else{
						if (Math.random() < chance)
						{
							double sld = straightLineDistance(i,q);
							m_distance[i][q] = sld + Math.random()*percentInc*sld;
							m_distance[q][i] = m_distance[i][q];
						}
					}
				}
			}
		}
	}
	
	/**
	 * Prints a CSV formated string containing the distances of the points.
	 * If two points are not connected, the distance is -1.
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param The OutputStream
	 */
	public void csvPrintDistance(OutputStream os)
	{
		PrintStream ps = new PrintStream(os);
		for(int i = 0; i < m_vertices.length;++i)
		{
			for(int j = 0; j < m_vertices.length;++j)
			{
				ps.print(m_distance[i][j]);
				if (j != m_vertices.length - 1)ps.print(",");
			}
			ps.println();
		}
		
	}
	/**
	 * Prints a CSV formated string containing the SL distances of the points.
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param The OutputStream
	 */
	public void csvPrintSLDistance(OutputStream os)
	{
		PrintStream ps = new PrintStream(os);
		for(int i = 0; i < m_vertices.length;++i)
		{
			for(int j = 0; j < m_vertices.length;++j)
			{
				ps.print(straightLineDistance(i,j));
				if (j != m_vertices.length - 1)ps.print(",");
			}
			ps.println();
		}
		
	}
	/**
	 * Prints a DOT formated string describing the graph.
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param The OutputStream
	 */
	public void dotPrint(OutputStream os)
	{
		PrintStream ps = new PrintStream(os);
		ps.println("Graph G{");
		for(int i = 0; i < m_vertices.length-1;++i)
		{
			for(int j = i+1; j < m_vertices.length;++j)
			{
				if (m_distance[i][j] == m_distance[j][i])
				{
					if (m_distance[i][j] >=0) ps.println(i + "--" + j
												 +" [label=\"" +m_distance[i][j]+"\"];");
				}else{
					if (m_distance[i][j] >=0) ps.println(i + "->" + j 
		                                         +" [label=\"" +m_distance[i][j]+"\"];");
					if (m_distance[j][i] >=0) ps.println(j + "->" + i 
		                                         +" [label=\"" +m_distance[j][i]+"\"];");
				}
			}
		}
		
		for (int i = 0; i < m_vertices.length;++i)
		{
			ps.println(i + "[pos=\""+m_vertices[i].getX()*100+","+m_vertices[i].getY()*100+"\"];");
		}
		ps.println("overlap=scale }");
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RoutingProblemGenerator2D rp = new RoutingProblemGenerator2D();
		rp.genVertices(100, 0, 1000, 0, 1000);
		rp.connect(0.3, 0.2);
		
		try{
			rp.dotPrint(new FileOutputStream("route.dot"));
			rp.csvPrintDistance(new FileOutputStream("distance.csv"));
			rp.csvPrintSLDistance(new FileOutputStream("sldistance.csv"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
