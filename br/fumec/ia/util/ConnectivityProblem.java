package br.fumec.ia.util;

/**
 * Class that solves the connectivity problem using the quickunion algorithm.
 * 
 * @author  Alair Dias Júnior
 * @version 1.0
 */
public class ConnectivityProblem {

	private int m_unions;
	private int m_vertices;
	private int[] m_id;
	
	/**
	 * Resets the problem.
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 */
	public void reset()
	{
		m_unions = 0;
		for (int i = 0; i < m_vertices; ++i)
			m_id[i]=i;
	}
	/**
	 * Class Constructor
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param The number of vertices of the problem 
	 */
	public ConnectivityProblem(int vertices)
	{
		m_vertices = vertices;
		m_unions = 0;
		m_id = new int[vertices];
		for (int i = 0; i < vertices; ++i)
			m_id[i]=i;
		
	}
	/**
	 * Connects two points. If the points where not otherwise connected before.
	 * If this is a new connection, returns true. False, otherwise.
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @param p The first vertice
	 * @param q The second vertice
	 * @return True if this is a new connection. False, otherwise.
	 */
	public boolean connect(int p, int q)
	{
		if (p < 0 || p >= m_vertices || q < 0 || q >= m_vertices)
			return false;
		
		int i,j;
	    for (i = p; i != m_id[i]; i = m_id[i]);
	    for (j = q; j != m_id[j]; j = m_id[j]);
	    if (i == j) return false; // Find
	    
	    ++m_unions;
	    m_id[i] = j; //Union
	    
	    return true;
	}
	
	/**
	 * Tests if all the vertices have been connected.
	 * 
	 * @author  Alair Dias Júnior
	 * @version 1.0
	 * @return True if all are connected. False, otherwise.
	 */
	public boolean connected()
	{
		if (m_unions == m_vertices - 1) return true;
		else return false;
	}
}
