package br.fumec.ia.search.npuzzle;

import java.util.LinkedList;

import br.fumec.ia.search.IAction;
import br.fumec.ia.search.IState;
import br.fumec.ia.search.SearchProblem;

public class PuzzleProblem implements SearchProblem {

	private PuzzleState state;
	
	public PuzzleProblem(int size)
	{
		state = new PuzzleState(size);
	}
	
	public PuzzleProblem(PuzzleState state)
	{
		this.state = new PuzzleState(state);
	}

	@Override
	public boolean isObjective(IState state) throws ClassCastException {
		
		PuzzleState pState = (PuzzleState)state;
		boolean ret = true;
		
		for (int i = 0; i < pState.getSize(); ++i)
			for (int j = 0; j < pState.getSize(); ++j)
				if (i == pState.getSize() - 1 && j == i)
				{
					ret = (ret && (pState.getNumber(i,j) == pState.EMPTY));
				}else{
					ret = (ret && (pState.getNumber(i,j) == i*pState.getSize() + j + 1));
				}
		
		return ret;
	}

	@Override
	public LinkedList<IAction> getValidActions(IState state)
			throws ClassCastException {
				
		LinkedList<IAction> successors = new LinkedList<IAction>();
		
		for (PuzzleState.Movement m : PuzzleState.Movement.values()) 
		{
			if (((PuzzleState)state).checkMove(m))
			{
				PuzzleAction a = new PuzzleAction(m);				
				successors.add(a);
			}
		}
		
		return successors;
	}

	@Override
	public IState execute(IState state, IAction action) {
		PuzzleState newState = new PuzzleState((PuzzleState)state);
		newState.move((PuzzleAction)action);
		return newState;
	}

	@Override
	public IState getInitialState() throws ClassCastException {
		return state;
	}
}
