package br.fumec.ia.search.npuzzle;

import br.fumec.ia.search.IAction;


public class PuzzleAction implements IAction {

	
	PuzzleState.Movement m_action;
	
	public PuzzleAction(PuzzleState.Movement m) {
		m_action = m;
	}
	
	public void setAction(PuzzleState.Movement action)
	{
		m_action = action;
	}
	
	public PuzzleState.Movement getAction()
	{
		return m_action;
	}
	
	@Override
	public Double getCost() {
		return 1.0;
	}

}
