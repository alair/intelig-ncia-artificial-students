package br.fumec.ia.search.apps.npuzzle;

import java.util.ArrayList;

import br.fumec.ia.search.AStarNodeComparator;
import br.fumec.ia.search.IFringe;
import br.fumec.ia.search.PQueue;
import br.fumec.ia.search.Search;
import br.fumec.ia.search.Solution;
import br.fumec.ia.search.npuzzle.PuzzleAction;
import br.fumec.ia.search.npuzzle.PuzzleProblem;
import br.fumec.ia.search.npuzzle.PuzzleState;

public class H2Search {
	
	public static void printSolution(Solution sol)
	{
		PuzzleAction a;
		while (sol.GetActions().size() > 0)
		{
			a = (PuzzleAction)sol.GetActions().pop();
			switch (a.getAction())
			{
				case UP:
					System.out.println("CIMA");
					break;
					
				case DOWN:
					System.out.println("BAIXO");
					break;
					
				case LEFT:
					System.out.println("ESQUERDA");
					break;
					
				case RIGHT:
					System.out.println("DIREITA");
					break;
			}
		}
	}
	
	public static void main(String[] args)
	{
		
		System.out.println("Resolvendo com Busca em Largura");
		System.out.println();
		
		Search search = new Search();
		
		ArrayList<Integer> a = new ArrayList<Integer>();
		a.add(0);a.add(2);a.add(4);
		a.add(7);a.add(5);a.add(3);
		a.add(8);a.add(1);a.add(6);
		
		PuzzleState state = new PuzzleState(a);
		state.setHeuristic(1);
		state.print(System.out);
		
		PuzzleProblem problem = new PuzzleProblem(state);
		
		IFringe  fringe = new PQueue(new AStarNodeComparator());
		long start = System.nanoTime();
		Solution sol = search.tree(problem, fringe, 0);
		long end = System.nanoTime();
		
		//printSolution(sol);
		PuzzleAction action;
		while (sol.GetActions().size() > 0)
		{
			action = (PuzzleAction)sol.GetActions().pop();
			state.move(action);
		}
	
		state.print(System.out);
		
		System.out.println();
		
		System.out.println("Tempo de Busca: " + (double)(end - start) / 1E9 + " s");
		//System.out.println("Nós Gerados: " + problem.getNumCopies());
		System.out.println("Profundidade da Solução: " + sol.getCost());
		System.out.println();
		
	}

}
