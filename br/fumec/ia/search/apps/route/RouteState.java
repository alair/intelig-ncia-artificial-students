package br.fumec.ia.search.apps.route;

import br.fumec.ia.search.IState;

public class RouteState implements IState {

	private Integer state = new Integer(-1);
	private Double eval = new Double(0.0);
	
	
	public final Integer getState() {
		return state;
	}


	public void setState(Integer state) {
		this.state = state;
	}


	public void setEval(Double eval) {
		this.eval = eval;
	}


	@Override
	public final Double eval() {
		return eval;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eval == null) ? 0 : eval.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RouteState other = (RouteState) obj;
		if (eval == null) {
			if (other.eval != null)
				return false;
		} else if (!eval.equals(other.eval))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}


	@Override
	public String toString() {
		
		switch(state)
		{
		case 0: return "Arad";
		case 1: return "Bucharest";
		case 2: return "Craiova";
		case 3: return "Dobreta";
		case 4: return "Eforie";
		case 5: return "Fagaras";
		case 6: return "Giurgiu";
		case 7: return "Hirsova";
		case 8: return "Iasi";
		case 9: return "Lugoj";
		case 10: return "Mehadia";
		case 11: return "Neamt";
		case 12: return "Oradea";
		case 13: return "Pitesti";
		case 14: return "RV";
		case 15: return "Sibiu";
		case 16: return "Timisoara";
		case 17: return "Urziceni";
		case 18: return "Vaslui";
		case 19: return "Zerind";

		}
		
		return state.toString();
	}
}
