package br.fumec.ia.search.apps.route;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;

import br.fumec.ia.search.IAction;
import br.fumec.ia.search.IState;
import br.fumec.ia.search.SearchProblem;
import br.fumec.ia.util.*;
import br.fumec.ia.util.CsvDBRead.InvalidDBFileException;


public class RouteProblem implements SearchProblem {
	
	private RouteState initial = new RouteState();
	private RouteState objective = new RouteState();
	private double[][] distances = null;
	private double[][] slDistances = null;
	
	public void setStates(Integer initial, Integer objective)
	{
		this.initial.setState(initial);
		this.objective.setState(objective);
		if (slDistances != null)
		{
			this.initial.setEval(slDistances[initial][objective]);
			this.objective.setEval(0.0);
		}
	}
	
	
	public RouteProblem(String distancesFile) 
			throws FileNotFoundException, IOException, InvalidDBFileException
	{
		CsvDBRead db = new CsvDBRead();
		db.readDB(distancesFile);
		distances = db.getInputs();
	}

	public RouteProblem(String distancesFile, String slDistancesFile) 
			throws FileNotFoundException, IOException, InvalidDBFileException
	{
		CsvDBRead db = new CsvDBRead();
		db.readDB(distancesFile);
		distances = db.getInputs();
	
		db.readDB(slDistancesFile);
		slDistances = db.getInputs();
	}

	
	@Override
	public boolean isObjective(IState state) throws ClassCastException {
		return state.equals(objective);
	}

	@Override
	public final IState getInitialState() throws ClassCastException {
		return initial;
	}

	@Override
	public LinkedList<IAction> getValidActions(IState state)
			throws ClassCastException {
		LinkedList<IAction> actions = new LinkedList<IAction>();
		RouteState routeState = (RouteState)state; 
		for (int i = 0; i < distances[routeState.getState()].length; ++i)
		{
			if (distances[routeState.getState()][i] > 0.0)
			{
				RouteAction routeAction = new RouteAction();
				routeAction.setDestiny(i);
				routeAction.setCost(distances[routeState.getState()][i]);
				actions.add(routeAction);		
			}
		}
		
		return actions;
	}

	@Override
	public IState execute(IState state, IAction action) {
		RouteState routeState = (RouteState)state;
		RouteAction routeAction = (RouteAction)action;
		Double distance = new Double(distances[routeState.getState()][routeAction.getDestiny()]);
		
		if (distance > 0.0)
		{
			RouteState newState = new RouteState();
			newState.setState(routeAction.getDestiny());
			if (slDistances != null)
			{
				newState.setEval(slDistances[routeAction.getDestiny()][objective.getState()]);
			}
			return newState;
		}
			
		return null;
	}
}
