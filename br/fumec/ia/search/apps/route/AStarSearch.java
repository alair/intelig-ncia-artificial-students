package br.fumec.ia.search.apps.route;

import br.fumec.ia.search.AStarNodeComparator;
import br.fumec.ia.search.PQueue;
import br.fumec.ia.search.Search;
import br.fumec.ia.search.Solution;
import br.fumec.ia.search.TreeNode;

public class AStarSearch {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			RouteProblem problem = new RouteProblem("files/distance.csv", "files/sldistance.csv");
			problem.setStates(9, 1);
			
			Search search = new Search();
			
			PQueue fringe = new PQueue(new AStarNodeComparator());
			
			Solution solution = search.graph(problem, fringe, 0);
			
			if(solution.GetResult() == Solution.Result.SOLUTION)
			{
				System.out.println("Cost: " + solution.getCost());
				System.out.println("Max Nodes In Memory: " + TreeNode.getMaxNodesInMemory());
				System.out.println("Generated Nodes: " + TreeNode.getGeneratedNodes());
			}else{
				System.out.println("Problema!");
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		

	}

}
