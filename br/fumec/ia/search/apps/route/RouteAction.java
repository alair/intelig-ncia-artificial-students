package br.fumec.ia.search.apps.route;

import br.fumec.ia.search.IAction;

public class RouteAction implements IAction {

	private Double cost = new Double (-1.0);
	private Integer destiny = new Integer(-1);
	
	@Override
	public final Double getCost() {
		return cost;
	}
	
	@Override
	public String toString()
	{
		return cost.toString();
	}

	public final Integer getDestiny() {
		return destiny;
	}

	public void setDestiny(Integer destiny) {
		this.destiny = destiny;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

}
