package br.fumec.ia.search.nqueens.app;

import br.fumec.ia.search.nqueens.NQueens;

public class QueenTest {

	public static void main(String[] args) {

		NQueens queens = new NQueens(1000000);
		queens.print(System.out);
		System.out.println();
		
		long start = System.nanoTime();
		queens.qs2();
		long end = System.nanoTime();
		System.out.println(((double)(end - start))/1E9 + " segundos");
		queens.print(System.out);
		if (!queens.noCollisions())
		{
			System.out.println("Erro no Algoritmo");
		}
		
		
	}

}
