package br.fumec.ia.game.tictactoe;

import br.fumec.ia.util.*;

/**
 * Provides the interface to interact with the Tic-Tac-Toe Game. 
 * 
 * @author  Alair Dias Júnior
 * @version 1.0
 */
public class TicTacToe
{
    /**
     * The width of the board.
     */
    private final int width = 3;
    /**
     * The height of the board.
     */
    private final int height = 3;

    // Movement Control (Move Only once)
    private int [] lastPPos = new int[2];
    private int player;
    private boolean moved;
    private int fHashCode = 0;

    // Internal data
    // -- board
    private Integer [][] board = new Integer[width][height];


    // Constructor
   /** 
    * Class constructor.
    */
    public TicTacToe()
    {

        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                board[i][j] = new Integer(-1);

        lastPPos[0] = -1;
        lastPPos[1] = -1;
        player = 0;
        moved = false;
    }

    /** 
    * Prints the game board.
    */

    public void printBoard()
    {
        for (int j = height - 1; j >= 0; --j)
        {
            System.out.print(j + "   ");
            for (int i = 0; i < width; ++i)
            {
                if (i != 0) System.out.print(" | ");
                if (board[i][j] == 0) System.out.print("X");
                else if (board[i][j] == 1) System.out.print("O");
                else System.out.print(" ");
            }
            System.out.println();
            System.out.println("   -----------");
         }   
         System.out.print(" ");
         for (int i = 0; i < width; ++i)
             System.out.print("   " + i);
         System.out.println();
    }


   /** 
    * Checks if the move is legal.
    * @param newPosX New column of the piece
    * @param newPosY New line of the piece
    * @return true if the move is legal. false otherwise.
    */
    public boolean checkLegalMove(int newPosX, int newPosY)
    {
        // checks the board outer bounds
        if (newPosX < 0 || newPosX >= width) return false;
        if (newPosY < 0 || newPosY >= height) return false;

        // checks if the square is empty
        if (board[newPosX][newPosY] != -1) return false;

        return true;
    }

   /** 
    * Puts the player mark in the indicated position
    * @param newPosX Column of the mark
    * @param newPosY Line of the mark
    * @return true if the move is legal. false otherwise.
    */
    public boolean move(int newPosX, int newPosY)
    {
        if (moved) return false;

        if (!checkLegalMove(newPosX, newPosY)) return false;

        // saves the last move
        lastPPos[0] = newPosX;
        lastPPos[1] = newPosY;

        // executes the move
        board[newPosX][newPosY] = player;

        moved = true;

        return true;
    }
    
   /** 
    * Undo the last move
    * @return true if the undo is legal. false otherwise.
    */
    public boolean undoMove()
    {
        if (!moved) return false;
        
        // undo the move
        board[lastPPos[0]][lastPPos[1]] = -1;

        moved = false;
        return true;
    }

   /** 
    * Checks if the game is over
    * @return The winner (0 --> p1; 1 --> p2; 3 --> tie).
    *         Returns -1 if it is not an end state.
    */
    public int checkEndState()
    {
        // checks the columns
        for (int i = 0; i < width; ++i)
            if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != -1) return board[i][0];

        // checks the lines
        for (int i = 0; i < height; ++i)
            if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != -1) return board[0][i];

        // checks the diagonals
        if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[1][1] != -1) return board[0][0];
        if (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[1][1] != -1) return board[0][2];

        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                if (board[i][j] == -1)
                {
                    return -1;
                }

      return 3;
    }

   /** 
    * Returns the board. Each element of the matrix represents
    * a square of the board. Value -1 indicates an empty square.
    * Value 0 indicates that player 1 has marked the square.
    * Value 1 indicates that player 2 has marked the square. 
    * @return A Matrix representing the game board.
    */
    public Integer [][] getBoardState()
    {
        Integer[][] brd = new Integer[width][height];
        for (int i = 0; i < width;++i)
            for (int j = 0; j < height;++j)
                brd[i][j] = board[i][j];
        return brd;
    }

   /** 
    * Returns the current player number
    * @return The number of the current player
    */
    public int getPlayerNumber()
    {
        return player;
    }

   /** 
    * Returns the width of the board
    * @return The width of the board
    */
    public int getWidth()
    {
        return width;
    }

   /** 
    * Returns the height of the board
    * @return The height of the board
    */
    public int getHeight()
    {
        return width;
    }

    /** 
     * checks if the player has moved
     * @return Returns true if the player has already moved
     */
     public boolean hasMoved()
     {
         return moved;
     }

    

   /** 
    * Copies the game state into a new object.
    * This method can be used during the Minimax algorithm in order to execute
    * the moves for the adversary. Actions took over the new object do not 
    * affect the original game state.
    * @param newPlayer The number of the player for the game copy
    * @return A copy of game, changing the turn to newPlayer's
    */
    public TicTacToe copy(int newPlayer)
    {
        TicTacToe game = new TicTacToe();
        game.player = newPlayer;
        for (int i = 0; i < width;++i)
            for (int j = 0; j < height;++j)
                game.board[i][j] = board[i][j];
        
        return game;
    }


  @Override public boolean equals(Object aThat)
    {
        //check for self-comparison
        if ( this == aThat ) return true;

        //use instanceof instead of getClass here for two reasons
        //1. if need be, it can match any supertype, and not just one class;
        //2. it renders an explicit check for "that == null" redundant, since
        //it does the check for null already - "null instanceof [type]" always
        //returns false. (See Effective Java by Joshua Bloch.)
        if ( !(aThat instanceof TicTacToe) ) return false;
        //Alternative to the above line :
        //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

        //cast to native object is now safe
        TicTacToe that = (TicTacToe)aThat;

        //now a proper field-by-field evaluation can be made
        if (this.player != that.player) return false;
        if (this.moved != that.moved) return false;
        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                if (this.board[i][j] != that.board[i][j]) return false;

        return true;
        
    }

    
    @Override public int hashCode() {
        //this style of lazy initialization is 
        //suitable only if the object is immutable
        if ( fHashCode == 0) 
        {
            int result = HashCodeUtil.SEED;
            result = HashCodeUtil.hash( result, player );
            result = HashCodeUtil.hash( result, moved );
            for (int i = 0; i < width; ++i)
                for (int j = 0; j < height; ++j)
                    result = HashCodeUtil.hash( result, board[i][j] );
                    
            fHashCode = result;
        }
        return fHashCode;
    }
    
}

