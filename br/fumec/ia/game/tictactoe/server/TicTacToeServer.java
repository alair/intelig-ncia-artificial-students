package br.fumec.ia.game.tictactoe.server;

import java.io.*;

import br.fumec.ia.game.tictactoe.*;
import br.fumec.ia.game.tictactoe.agents.BasicAgent;



public class TicTacToeServer 
{

    private static TicTacToe game;
    private static BasicAgent [] player = new BasicAgent[2];

    private static void printWelcome()
    {
        System.out.println("");
        System.out.println("=============================");
        System.out.println("         TicTacToe Game");
        System.out.println("=============================");
        System.out.println("");
    }


    public static void main(String[] args) 
    {
        printWelcome();
        ClassLoader classLoader = TicTacToeServer.class.getClassLoader();
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 2; ++i)
        {
            while(true)
            {
                try 
                {
                    System.out.print("Enter agent for player " + (i+1) + ": ");
                    String agentName = input.readLine();
                    player[i] = (BasicAgent) classLoader.loadClass("br.fumec.ia.game.tictactoe.agents." + agentName).newInstance();
                    break;
                    
                }catch (Exception e) {
                    System.out.println("Invalid Agent Class.");
                }
            }
        }

        game = new TicTacToe();
        play();

        System.out.println("");

    }

    private static void play()
    {
        try 
        {
            int winner = -1;
            int round = 0;
            boolean gaveUp = false;
            System.out.println("Let the Match Begins");
            System.out.println();
            game.printBoard();
            while(true)
            {
                if (!player[0].move(game))  {winner = 1; gaveUp = true; break;};
                if ((winner = game.checkEndState()) != -1) {break;}
                game = game.copy(1);
                System.out.println();
                System.out.println("Round " + (++round) + ". Player 1 moved.");
                System.out.println();
                game.printBoard();
                Thread.sleep(2000);//sleep for 2000 ms
                if (!player[1].move(game))  {winner = 0; gaveUp = true; break;};
                if ((winner = game.checkEndState()) != -1) {break;}
                game = game.copy(0);
                System.out.println();
                System.out.println("Round " + (round) + ". Player 2 moved.");
                System.out.println();
                game.printBoard();
                Thread.sleep(2000);//sleep for 2000 ms
            }
            System.out.println();
            System.out.println("Final Result:");
            System.out.println();
            game.printBoard();
            System.out.println();
            if (winner != 3) System.out.println("Player " + (winner+1) + " Wins!!!");
            else  System.out.println("It is a Tie!");
            if (gaveUp) System.out.println("Other player has gave up");

        }catch (Exception e) {
            e.printStackTrace();
        }

    }

}
