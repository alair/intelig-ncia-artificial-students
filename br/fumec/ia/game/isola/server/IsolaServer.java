package br.fumec.ia.game.isola.server;

import java.io.*;

import br.fumec.ia.game.isola.*;
import br.fumec.ia.game.isola.agents.BasicAgent;



public class IsolaServer 
{

    private static Isola game;
    private static BasicAgent [] player = new BasicAgent[2];
    private static long TIME_OUT = 30000;
    
    private static void printWelcome()
    {
        System.out.println("");
        System.out.println("=============================");
        System.out.println("         Isola Game");
        System.out.println("=============================");
        System.out.println("");
    }


    public static void main(String[] args) 
    {
        printWelcome();
        ClassLoader classLoader = IsolaServer.class.getClassLoader();
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < 2; ++i)
        {
            while(true)
            {
                try 
                {
                    System.out.print("Enter agent for player " + (i+1) + ": ");
                    String agentName = input.readLine();
                    player[i] = (BasicAgent) classLoader.loadClass("ia.game.isola.agents." + agentName).newInstance();
                    break;
                    
                }catch (Exception e) {
                    System.out.println("Invalid Agent Class.");
                }
            }
        }

        int size = 7;
        try 
        {
            System.out.print("Enter the size of the Board: ");
            size = Integer.parseInt(input.readLine());
        }catch(Exception e)
        {
            System.out.println("Invalid size, assuming default size");
        }
        finally
        {
            game = new Isola(size, size);
            play();

            System.out.println("");
        }


    }

    private static void play()
    {
        try 
        {
        	
            int winner = -1;
            int round = 0;
            boolean gaveUp = false;
            boolean timeOut = false;
            System.out.println("Let the Match Begins");
            game.printBoard();
            AgentPlayer ap = new AgentPlayer();
            
            while(!gaveUp)
            {
            	ap.setAgent(player[0], game);
            	Thread t1 = new Thread(ap);
            	t1.start();
            	synchronized(t1)
            	{
            		t1.wait(TIME_OUT);
            	}
            	gaveUp = ap.hasGaveUp();
            	timeOut = ap.hasTimedOut();
            	
            	if (gaveUp) 
                {
            		winner = 1;
            		break;
                }
            	
                if (!game.checkCanMove(1)) {winner = 0; break;}
                game = game.copy(1);
                System.out.println("");
                System.out.println("Round " + (++round) + ". Player 1 moved.");
                game.printBoard();
                Thread.sleep(2000);//sleep for 2000 ms
                
            	ap.setAgent(player[1], game);
            	Thread t2 = new Thread(ap);
            	t2.start();
            	synchronized(t2)
            	{
            		t2.wait(TIME_OUT);
            	}
            	gaveUp = ap.hasGaveUp();
            	timeOut = ap.hasTimedOut();
            	
            	if (gaveUp)	
                {
            		winner = 0;
            		break;
                }              
               
                if (!game.checkCanMove(0)) {winner = 1; break;}
                game = game.copy(0);
                System.out.println("");
                System.out.println("Round " + (round) + ". Player 2 moved.");
                game.printBoard();
                Thread.sleep(2000);//sleep for 2000 ms
            }
            System.out.println("");
            game.printBoard();
            System.out.println("Player " + (winner+1) + " Wins!!!");
            if (gaveUp) System.out.println("Other player has gave up");
            if (timeOut) System.out.println("Other player has run out of time");

        }catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    private static class AgentPlayer implements Runnable
    {
    	private BasicAgent m_agent;
    	private boolean m_gaveUp;
    	private Isola m_game;
    	private boolean m_timeOut;
    	
    	public void setAgent(BasicAgent agent, Isola game)
    	{
    		m_agent = agent;
    		m_game = game;
    		m_gaveUp = false;
    		m_timeOut = true;
    	}
    	
    	public void run()
    	{
    		m_gaveUp = !m_agent.move(m_game) || !game.hasRemoved();
    		m_timeOut = false;
    	}
    	
    	public boolean hasGaveUp()
    	{
    		return m_gaveUp;
    	}
    	
    	public boolean hasTimedOut()
    	{
    		return m_timeOut;
    	}
    }

}
