package br.fumec.ia.game.isola.agents;

import br.fumec.ia.game.isola.*;
import br.fumec.ia.game.isola.agents.BasicAgent;

/**
 * This is the template for implementing the Game Agents.
 * You should use this template to implement your agent class.
 * You may create other methods and attributes, but should not
 * alter the package neither the signature of the method move.
 * @author  Alair Dias Júnior
 * @version 1.0
 */
public class TemplateAgent implements BasicAgent
{
    public boolean move(final Isola game)
    {

        return true;
    }
}
