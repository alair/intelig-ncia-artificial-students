package br.fumec.ia.game.isola;

import br.fumec.ia.util.*;

/**
 * Provides the interface to interact with the Isola Game. 
 * 
 * @author  Alair Dias Júnior
 * @version 1.1
 */
public class Isola
{
    // Finals
    /**
     * The width of the board.
     */
    private int width = 7;
    /**
     * The height of the board.
     */
    private int height = 7;

    // Movement Control (Move Only once)
    private int [] lastPPos = new int[2];
    private int [] lastSquareRemoved = new int[2];
    private int player;
    private boolean moved;
    private boolean removed;
    private int fHashCode = 0;

    // Internal data
    // -- board
    private boolean [][] board;;
    // -- removed square list
    //private HashMap<Square,Boolean> hashBoard = new HashMap<Square,Boolean>();
    // -- Players Positions
    private int [][] playerPos = new int[2][2];


    // Constructor
   /** 
    * Class constructor.
    * @param boardWidth The width of the board.
    * @param boardHeight The height of the board.
    */
    public Isola(int boardWidth, int boardHeight)
    {
        width = boardWidth;
        height = boardHeight;
        board = new boolean [width][height];

        int p1StartX = width / 2;
        int p1StartY = 0;
        int p2StartX = width / 2;
        int p2StartY = height - 1;


        playerPos[0][0] = p1StartX;
        playerPos[0][1] = p1StartY;
        playerPos[1][0] = p2StartX;
        playerPos[1][1] = p2StartY;
        lastPPos[0] = p1StartX;
        lastPPos[1] = p1StartY;
        player = 0;
        lastSquareRemoved[0] = -1;
        lastSquareRemoved[1] = -1;
        board[p1StartX][p1StartY] = true;
        board[p2StartX][p2StartY] = true;
        //hashBoard.put(new Square(p1StartX, p1StartY), true);
        //hashBoard.put(new Square(p2StartX, p2StartY), true);
        moved = false;
        removed = false;
    }

    /** 
    * Prints the game board.
    */

    public void printBoard()
    {
        System.out.print("   -");
        for (int i = 0; i < width; ++i)
            System.out.print("----");
        System.out.println();
        for (int j = height - 1; j >= 0; --j)
        {
            System.out.print(j + "  ");
            for (int i = 0; i < width; ++i)
            {
                System.out.print("| ");
                if (i == playerPos[1][0] && j == playerPos[1][1]) System.out.print("2 ");
                else if (i == playerPos[0][0] && j == playerPos[0][1]) System.out.print("1 ");
                else
                    if (board[i][j]) System.out.print("X ");
                    //if (hashBoard.containsKey(new Square(i,j))) System.out.print("X ");
                    else System.out.print("  ");
            }
            System.out.println("|");
            System.out.print("   -");
            for (int i = 0; i < width; ++i)
                System.out.print("----");
            System.out.println();
         }   
         System.out.print("  ");
         for (int i = 0; i < width; ++i)
             System.out.print("   " + i);
         System.out.println();
    }


   /** 
    * Checks if the new position is legal.
    * @param newPosX New column of the piece
    * @param newPosY New line of the piece
    * @return true if the move is legal. false otherwise.
    */
    public boolean checkLegalMove(int newPosX, int newPosY)
    {
        // checks the board outer bounds
        if (newPosX < 0 || newPosX >= width) return false;
        if (newPosY < 0 || newPosY >= height) return false;

        // checks the range of movement
        if (Math.abs(newPosX - playerPos[player][0]) > 1) return false;
        if (Math.abs(newPosY - playerPos[player][1]) > 1) return false;

        // checks if really moved or not moved to opponents position
        if (newPosX == playerPos[0][0] && newPosY == playerPos[0][1]) return false;
        if (newPosX == playerPos[1][0] && newPosY == playerPos[1][1]) return false;

        // checks if the square is there
        if (board[newPosX][newPosY]) return false;
        //if (hashBoard.containsKey(new Square(newPosX,newPosY))) return false;

        return true;
    }

   /** 
    * Checks if the square can be removed
    * @param squareX Column of the square
    * @param squareY Line of the square
    * @return true if the removal is legal. false otherwise.
    */
    public boolean checkLegalRemoval(int squareX, int squareY)
    {
        // checks the board outer bounds
        if (squareX < 0 || squareX >= width) return false;
        if (squareY < 0 || squareY >= height) return false;

        // checks if the square is there
        if (board[squareX][squareY]) return false;
        //if (hashBoard.containsKey(new Square(squareX,squareY))) return false;

        // checks if no player is on the square
        if ((playerPos[0][0] == squareX && playerPos[0][1] == squareY) || 
            (playerPos[1][0] == squareX && playerPos[1][1] == squareY)) return false;

        return true;
    }

   /** 
    * Move the palyer's piece to a new position
    * @param newPosX New column of the piece
    * @param newPosY New line of the piece
    * @return true if the move is legal. false otherwise.
    */
    public boolean move(int newPosX, int newPosY)
    {
        if (moved) return false;

        if (!checkLegalMove(newPosX, newPosY)) return false;

        // saves the last position
        lastPPos[0] = playerPos[player][0];
        lastPPos[1] = playerPos[player][1];

        // gets the new position
        playerPos[player][0] = newPosX;
        playerPos[player][1] = newPosY;
        moved = true;
        return true;
    }
    
   /** 
    * Undo the last move
    * This method will call undoRemoval() if a removal has been done.
    * @return true if the undo is legal. false otherwise.
    */
    public boolean undoMove()
    {
        if (!moved) return false;
        undoRemoval(); // First the removed square must be positioned again
        
        // gets the last position
        playerPos[player][0] = lastPPos[0];
        playerPos[player][1] = lastPPos[1];

        moved = false;
        return true;
    }

   /** 
    * Removes the square
    * @param squareX Column of the square
    * @param squareY Line of the square
    * @return true if the removal is legal. false otherwise.
    */
    public boolean remove(int squareX, int squareY)
    {
        if (removed || !moved) return false;
        if (!checkLegalRemoval(squareX, squareY)) return false;

        board[squareX][squareY] = true;
        //hashBoard.put(new Square(squareX,squareY), true);
        lastSquareRemoved[0] = squareX;
        lastSquareRemoved[1] = squareY;

        removed = true;
        return true;
    }

   /** 
    * Undo the last removal
    * @return true if the undo is legal. false otherwise.
    */
    public boolean undoRemoval()
    {
        if (!removed) return false;

        board[lastSquareRemoved[0]][lastSquareRemoved[1]] = false;
        //hashBoard.remove(new Square(lastSquareRemoved[0],lastSquareRemoved[1]));
        removed = false;
        return true;
    }

   /** 
    * Checks if a player has legal moves
    * @param player The number of the player (0 for p1; 1 for p2)
    * @return true if the player has a legal move. false otherwise.
    */
    public boolean checkCanMove(int player)
    {
        // check if the player cannot move
        for (int l = -1; l < 2; ++l)
            for (int k = -1; k < 2; ++k)
            {
                int x = playerPos[player][0] + l;
                int y = playerPos[player][1] + k;
                if (x >=0 && x < width && y >= 0 && y < height && (!board[x][y]) &&
                    (playerPos[0][0] != x || playerPos[0][1] != y) &&
                    (playerPos[1][0] != x || playerPos[1][1] != y)) return true;
            }

      return false;
    }

   /** 
    * Returns the player current position
    * @param player The number of the player (0 for p1; 1 for p2)
    * @return An array of int. 0-> Column; 1-> line.
    */
    public int[] getPlayerPos(int player)
    {
        int [] pos = new int [2];
        pos[0] = playerPos[player][0];
        pos[1] = playerPos[player][1];
        return pos;
    }

   /** 
    * Returns the board removed squares
    * @return A Matrix representing the game board.
    *         Each position indicates if a square is present (false) or removed (true).
    */
    public boolean [][] getBoardState()
    {
        boolean[][] brd = new boolean[width][height];
        for (int i = 0; i < width;++i)
            for (int j = 0; j < height;++j)
                brd[i][j] = board[i][j];
        return brd;
    }

   /** 
    * Returns the current player number
    * @return The number of the current player
    */
    public int getPlayerNumber()
    {
        return player;
    }

   /** 
    * Returns the width of the board
    * @return The width of the board
    */
    public int getWidth()
    {
        return width;
    }

   /** 
    * Returns the height of the board
    * @return The height of the board
    */
    public int getHeight()
    {
        return width;
    }

    /** 
     * checks if the player has moved
     * @return Returns true if the player has already moved
     */
     public boolean hasMoved()
     {
         return moved;
     }
     
     /** 
      * checks if the player has removed a square
      * @return Returns true if the player has already removed a square
      */
      public boolean hasRemoved()
      {
          return removed;
      }
   /** 
    * Copies the game state into a new object.
    * This method can be used during the Minimax algorithm in order to execute
    * the moves for the adversary. Actions taked over the new object do not 
    * afect the original game state.
    * @param newPlayer The number of the player for the game copy
    * @return A copy of game, changing the turn to newPlayer's
    */
    public Isola copy(int newPlayer)
    {
        Isola game = new Isola(width, height);
        game.player = newPlayer;
        for (int i = 0; i < width;++i)
            for (int j = 0; j < height;++j)
                game.board[i][j] = board[i][j];
        //game.hashBoard.putAll(hashBoard);

        for (int i = 0; i < 2; ++i)
            for (int j = 0; j < 2; ++j)
                game.playerPos[i][j] = playerPos[i][j];
        
        return game;
    }


  @Override public boolean equals(Object aThat)
    {
        //check for self-comparison
        if ( this == aThat ) return true;

        //use instanceof instead of getClass here for two reasons
        //1. if need be, it can match any supertype, and not just one class;
        //2. it renders an explict check for "that == null" redundant, since
        //it does the check for null already - "null instanceof [type]" always
        //returns false. (See Effective Java by Joshua Bloch.)
        if ( !(aThat instanceof Isola) ) return false;
        //Alternative to the above line :
        //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

        //cast to native object is now safe
        Isola that = (Isola)aThat;

        //now a proper field-by-field evaluation can be made
        if (this.player != that.player) return false;
        if (this.moved != that.moved) return false;
        if (this.removed != that.removed) return false;
        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                if (this.board[i][j] != that.board[i][j]) return false;
        //if (!this.hashBoard.equals(that.hashBoard)) return false;

        for (int i = 0; i < 2; ++i)
            for (int j = 0; j < 2; ++j)
                if (this.playerPos[i][j] != that.playerPos[i][j]) return false;

        return true;
        
    }

    
    @Override public int hashCode() {
        //this style of lazy initialization is 
        //suitable only if the object is immutable
        if ( fHashCode == 0) 
        {
            int result = HashCodeUtil.SEED;
            result = HashCodeUtil.hash( result, player );
            result = HashCodeUtil.hash( result, moved );
            result = HashCodeUtil.hash( result, removed );
            for (int i = 0; i < width; ++i)
                for (int j = 0; j < height; ++j)
                    result = HashCodeUtil.hash( result, board[i][j] );
            //result = HashCodeUtil.hash( result, hashBoard );

            for (int i = 0; i < 2; ++i)
                for (int j = 0; j < 2; ++j)
                    result = HashCodeUtil.hash( result, playerPos[i][j] );
                    
            fHashCode = result;
        }
        return fHashCode;
    }
    
}

