package br.fumec.ia.game.monochrome;

public class DFSearch {

	
	private static void printSolution(Solution sol)
	{
		MonochromeAction act;
		while ((act = (MonochromeAction) sol.getNextAction()) != null)
		{
			System.out.print(act.getAction() + " ");
		}
		System.out.println();
		System.out.println("Custo: " + sol.getCost());
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
		
			Fringe franja = new Stack();
			MonochromeProblem problem = new MonochromeProblem();
						
			GraphSearch search = new GraphSearch();
			long time = System.currentTimeMillis();
			Solution sol = search.execute(problem, franja,0);
			time = System.currentTimeMillis() - time;
			System.out.println("Tempo de Execução: " + time + " ms");
			printSolution(sol);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
