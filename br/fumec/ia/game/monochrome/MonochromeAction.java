package br.fumec.ia.game.monochrome;

public class MonochromeAction implements Action {

	int m_action;
	
	public void setAction(int action)
	{
		m_action = action;
	}
	
	public int getAction()
	{
		return m_action;
	}
	
	@Override
	public double getCost() {
		return 1;
	}

}
