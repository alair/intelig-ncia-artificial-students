package br.fumec.ia.game.monochrome;

public class IDFSearch {

	
	private static void printSolution(Solution sol)
	{
		MonochromeAction act;
		while ((act = (MonochromeAction) sol.getNextAction()) != null)
		{
			System.out.print(act.getAction() + " ");
		}
		System.out.println();
		System.out.println("Custo: " + sol.getCost());
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
		
			Fringe franja = new Stack();
			MonochromeProblem problem = new MonochromeProblem();
						
			long time = System.currentTimeMillis();
			Solution sol = new Solution();
			int depth = 1;
			do 
			{
				System.out.print(depth + " ");
				sol = new GraphSearch().execute(problem, franja,depth++);
			}while (sol.result() == Solution.Result.CUTOFF);
			
			System.out.println();
			time = System.currentTimeMillis() - time;
			System.out.println("Tempo de Execução: " + time + " ms");
			printSolution(sol);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
