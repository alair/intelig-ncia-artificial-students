package br.fumec.ia.game.monochrome;
import java.util.ArrayList;


public class MonochromeProblem implements SearchProblem {

	public final int VAZIO = 0;
	public final int AZUL = 1;
	public final int AMARELO = 2;
	public final int VERDE = 3;
	public final int VERMELHO = 4;
	
	@Override
	public State getInitialState() throws ClassCastException {
		MonochromeState state = new MonochromeState(3);

		// Linha 1
		state.setPosition(0, 0, VERMELHO);
		state.setPosition(0, 1, VERDE);
		state.setPosition(0, 2, AZUL);

		// Linha 2
		state.setPosition(1, 0, VERMELHO);
		state.setPosition(1, 1, VAZIO);
		state.setPosition(1, 2, AZUL);
		
		// Linha 3
		state.setPosition(2, 0, AZUL);
		state.setPosition(2, 1, VERDE);
		state.setPosition(2, 2, AZUL);
	
		
		return state;
	}

	@Override
	public ArrayList<Successor> getSuccessor(State state)
			throws ClassCastException {
		ArrayList<Successor> suc = new ArrayList<Successor>();
		
		MonochromeState st = (MonochromeState)state;
		
		for (int i = 0; i < 4; ++i)
		{
			MonochromeState next = st.copy();
			if (next.move(i))
			{
				MonochromeAction act = new MonochromeAction();
				act.setAction(i);
				Successor s = new Successor();
				s.setAction(act);
				s.setState(next);
				suc.add(s);
			}
		}
		
		return suc;
	}

	@Override
	public boolean isObjective(State state) throws ClassCastException {
		
	
		MonochromeState st = (MonochromeState)state;
		
		int line = 0;
		int col = 0;
		
		while(col < st.getState().length)
		{
			boolean moved = false;
			if (st.getState()[line][col] != AZUL) return false;
			while(line > 0 && st.getState()[line-1][col] == AZUL)
			{
				moved = true;
				line--;
			}
			
			while(!moved && line < st.getState().length - 1 && st.getState()[line+1][col] == AZUL)
				line++;
			
			++col;
		}

		if (st.getState()[line][col-1] != AZUL) return false;
		
		if (line != 1) return false;
		
		return true;
	}

}
