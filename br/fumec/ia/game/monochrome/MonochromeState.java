package br.fumec.ia.game.monochrome;


import java.util.Arrays;

public class MonochromeState implements State {

	public final int UP = 0;
	public final int DOWN = 1;
	public final int RIGHT = 2;
	public final int LEFT = 3;
	
	private int m_board[][];
	
	private int emptyLine = 0;
	private int emptyCol = 0;
	
	public MonochromeState copy()
	{
		MonochromeState cp = new MonochromeState(m_board.length);
		
		for (int i = 0; i < m_board.length; ++i)
			for (int j = 0; j < m_board.length; ++j)
				cp.setPosition(i, j, m_board[i][j]);
		
		return cp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(m_board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonochromeState other = (MonochromeState) obj;
		
		for(int i = 0; i < m_board.length; ++i)
			for(int j = 0; j < m_board.length; ++j)
				if (m_board[i][j] != other.m_board[i][j]) return false;
		
		return true;
	}

	public MonochromeState(int side)
	{
		m_board = new int[side][side];
		
		for (int i = 0; i < side; ++i)
		{
			for (int j = 0; j < side; ++j)
				m_board[i][j] = 0;
		}
	}
	
	public boolean move(int m)
	{
		switch(m)
		{
		case DOWN:
			if (emptyLine > 0)
			{
				m_board[emptyLine][emptyCol] = m_board[emptyLine-1][emptyCol];
				m_board[emptyLine-1][emptyCol] = 0;
				emptyLine--;
				return true;
			}
			break;
			
		case UP:
			if (emptyLine < m_board.length - 1)
			{
				m_board[emptyLine][emptyCol] = m_board[emptyLine+1][emptyCol];
				m_board[emptyLine+1][emptyCol] = 0;
				emptyLine++;
				return true;
			}
			break;
			
		case RIGHT:
			if (emptyCol > 0)
			{
				m_board[emptyLine][emptyCol] = m_board[emptyLine][emptyCol-1];
				m_board[emptyLine][emptyCol-1] = 0;
				emptyCol--;
				return true;
			}
			break;
			
		case LEFT:
			if (emptyCol < m_board.length - 1)
			{
				m_board[emptyLine][emptyCol] = m_board[emptyLine][emptyCol+1];
				m_board[emptyLine][emptyCol+1] = 0;
				emptyCol++;
				return true;
			}
			break;
		

		}
		return false;
	}
	
	public void setPosition(int linha, int coluna, int colour)
	{
		if (linha >= 0 && linha < m_board.length && coluna >= 0 && coluna < m_board.length)
		{
			m_board[linha][coluna] = colour;
			if (colour == 0)
			{
				emptyLine = linha;
				emptyCol = coluna;
			}
		}
	}
	
	public final int[][] getState()
	{
		return m_board;
	}
	
	@Override
	public double eval() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
