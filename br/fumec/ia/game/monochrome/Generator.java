package br.fumec.ia.game.monochrome;

import java.util.Random;

public class Generator {

	enum tColour
	{
		empty,
		red,
		blue,
		green,
		yellow
	};
	
	enum tMovement
	{
		up,
		down,
		right,
		left
	};
	
	tColour board[][];
	int initialLine = 0;
	int endLine = 0;
	int emptyLine = 0;
	int emptyCol = 0;
	
	public static tMovement[] Movements = tMovement.values();
	public static tColour[] Colours = tColour.values();
	
	public void makeBridge(int side, tColour solColour)
	{	
		Random rnd = new Random();
		initialLine = rnd.nextInt(side);
		endLine = rnd.nextInt(side);
		
		int beg = initialLine > endLine ? endLine: initialLine;
		int end = initialLine > endLine ? initialLine : endLine;
		
		for (int i = 0; i < side; ++i)
		{
			board[initialLine][i] = solColour;
		}
		
		for (int i = beg+1; i <= end; ++i )
		{
			board[i][side - 1] = solColour;
		}
	}
	
	public void newSolution(int side, tColour solColour)
	{	
		board = new tColour[side][side];
		for (int i = 0; i < side; ++i)
		{
			for (int j = 0; j < side; ++j)
				board[i][j] = tColour.empty;
		}	
		
		makeBridge(side, solColour);
		
		boolean empty = false;
		
		Random rnd = new Random();
		
		for (int i = 0; i < side; ++i)
		{
			for (int j = 0; j < side; j++)
			{
				if (board[i][j] == solColour) continue;
				
				boolean filled = false;
				while (!filled)
				{
					int index = rnd.nextInt(Colours.length);
					tColour c = tColour.class.getEnumConstants()[index];
					if (c == solColour) continue;
					if (c == tColour.empty) continue;
					board[i][j] = c;
					filled = true;
				}
			}
		}
		while (!empty)
		{
			int i = rnd.nextInt(side);
			int j = rnd.nextInt(side);
			
			if (board[i][j] != solColour)
			{
				board[i][j] = tColour.empty;
				empty = true;
				emptyLine = i;
				emptyCol = j;

			}
		}
		
	}
	
	public void print()
	{
		for (int i = 0; i < board.length; ++i)
		{
			for (int j = 0; j < board.length; ++j)
			{
				char c = 'X';
				switch(board[i][j])
				{
				case empty:
					c='e';
					break;
				case red:
					c = 'r';
					break;
				case blue:
					c = 'b';
					break;
				case green:
					c = 'g';
					break;
				case yellow:
					c = 'y';
					break;
				}
				
				System.out.print(c + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	
	public boolean move(tMovement move)
	{
		boolean ret = false;
		
		switch(move)
		{
		case up:
			if (emptyLine > 0)
			{
				board[emptyLine][emptyCol] = board[emptyLine-1][emptyCol];
				board[emptyLine-1][emptyCol] = tColour.empty;
				emptyLine--;
				ret = true;
			}
			break;
			
		case down:
			if (emptyLine < board.length - 1)
			{
				board[emptyLine][emptyCol] = board[emptyLine+1][emptyCol];
				board[emptyLine+1][emptyCol] = tColour.empty;
				emptyLine++;
				ret = true;
			}
			break;
			
		case left:
			if (emptyCol > 0)
			{
				board[emptyLine][emptyCol] = board[emptyLine][emptyCol-1];
				board[emptyLine][emptyCol-1] = tColour.empty;
				emptyCol--;
				ret = true;
			}
			break;
			
		case right:
			if (emptyCol < board.length - 1)
			{
				board[emptyLine][emptyCol] = board[emptyLine][emptyCol+1];
				board[emptyLine][emptyCol+1] = tColour.empty;
				emptyCol++;
				ret = true;
			}
			break;
			
		}
		
		return ret;
	}
	
	public void shuffle()
	{
		int i = 0;
		Random rnd = new Random();
		while (i < board.length * 1000000)
			if (move(tMovement.class.getEnumConstants()[rnd.nextInt(Movements.length)]))
				++i;
				
	}
	
	public static void main(String[] args) {
		Generator g = new Generator();
		g.newSolution(3, tColour.blue);
		g.print();
		g.shuffle();
		g.print();
	}

}
