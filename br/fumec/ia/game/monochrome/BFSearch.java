package br.fumec.ia.game.monochrome;

public class BFSearch {

	
	private static void printSolution(Solution sol)
	{
		MonochromeAction act;
		while ((act = (MonochromeAction) sol.getNextAction()) != null)
		{
			System.out.print(act.getAction() + " ");
		}
		System.out.println();
		System.out.println("Custo: " + sol.getCost());
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
		
			Fringe franja = new Queue();
			MonochromeProblem problem = new MonochromeProblem();
			
			TreeSearch search = new TreeSearch();
			long time = System.currentTimeMillis();
			Solution sol = search.execute(problem, franja,0);
			time = System.currentTimeMillis() - time;
			System.out.println("Tempo de Execução: " + time + " ms");
			printSolution(sol);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
