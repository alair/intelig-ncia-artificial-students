package br.fumec.ia.maze;

public enum Movements
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}