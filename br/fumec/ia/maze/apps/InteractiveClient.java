package br.fumec.ia.maze.apps;


import java.io.Console;
import javax.xml.soap.*;
import br.fumec.ia.maze.client.MazeConnection;
import br.fumec.ia.maze.Maze;

public class InteractiveClient {
	 
    private static final String localEndpoint = "http://localhost:7890/endpoint";
 
    public static void main(String[] args) throws SOAPException {
        try{
            
            String endpoint = null;
            if (args.length > 0)
                endpoint = args[0];
            else
                endpoint = localEndpoint;
                
                
    	
            MazeConnection con = new MazeConnection();
    		System.out.println("--------------------------------------------");
    		System.out.println("Resultado do getState");
    		System.out.println(con.getState(endpoint));
    		System.out.println("--------------------------------------------");
    		Console cons = System.console();
    		char command;
    		do
    		{
    		    System.out.println();
    		    System.out.println("Escolha uma opção:");
    		    System.out.println("\t c: move para cima");
    		    System.out.println("\t b: move para baixo");
    		    System.out.println("\t e: move para a esquerda");
    		    System.out.println("\t d: move para a direita");
    		    System.out.println("\t s: executa uma chamada getState");
    		    System.out.println("\t q: encerra o programa");

   	            command = cons.readLine().charAt(0);

    		    switch(command)
    		    {
    		    case 'c':
    		        System.out.println("--------------------------------------------");
    		        System.out.println("Resultado do move");
    		        System.out.println(con.move(endpoint, Maze.Movements.UP));
    		        System.out.println("--------------------------------------------");
    		        break;
    		    case 'b':
    		        System.out.println("--------------------------------------------");
                    System.out.println("Resultado do move");
                    System.out.println(con.move(endpoint, Maze.Movements.DOWN));
                    System.out.println("--------------------------------------------");
                    break;
    		    case 'e':
    		        System.out.println("--------------------------------------------");
                    System.out.println("Resultado do move");
                    System.out.println(con.move(endpoint, Maze.Movements.LEFT));
                    System.out.println("--------------------------------------------");
                    break;
    		    case 'd':
    		        System.out.println("--------------------------------------------");
                    System.out.println("Resultado do move");
                    System.out.println(con.move(endpoint, Maze.Movements.RIGHT));
                    System.out.println("--------------------------------------------");
                    break;
    		    case 's':
    		        System.out.println("--------------------------------------------");
    	            System.out.println("Resultado do getState");
    	            System.out.println(con.getState(endpoint));
    	            System.out.println("--------------------------------------------");
    	            break;
    		    }
    		}while(command != 'q');
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
}