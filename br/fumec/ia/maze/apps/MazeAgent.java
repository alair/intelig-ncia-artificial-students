package br.fumec.ia.maze.apps;

import br.fumec.ia.maze.agents.MazeProblem;
import br.fumec.ia.search.*;

public class MazeAgent {

    private static final String localEndpoint = "http://localhost:7890/endpoint";
    //private static final String localEndpoint = "http://172.16.4.41:7890/endpoint";
    
    public static void main(String[] args) {
        
        MazeProblem problem = new MazeProblem(localEndpoint);
        Search search = new Search();
        
        //Queue fringe = new Queue();
        //Stack fringe = new Stack();
        //PQueue fringe = new PQueue(new GreedNodeComparator());
        PQueue fringe = new PQueue(new AStarNodeComparator()); 
        if (search.graph(problem, fringe, 0).GetResult() == Solution.Result.SOLUTION)
            problem.goToState(problem.getExit());
    }

}
