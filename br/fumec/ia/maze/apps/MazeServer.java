package br.fumec.ia.maze.apps;


import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Random;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.events.*;
import br.fumec.ia.maze.Maze;
import br.fumec.ia.maze.Room;
import br.fumec.ia.maze.server.MazeWebService;
import br.fumec.ia.maze.view.MazeAgentTracker;
import br.fumec.ia.maze.view.MazeView;


public class MazeServer{
    
    private static Display display;
    private static Shell mazeShell;
    private static Shell newMazeDialog;
    private static Composite mazeComposite;
    private static Maze maze;
    private static MazeView mazeView;
    private static MazeAgentTracker agentTracker;
    private static MazeWebService webService;
    
    private static void closeServer()
    {
        mazeShell.dispose();
    }
    
    private static Integer[] getNewMazeSize()
    {
        final int[] width = new int[1];
        final int[] height = new int[1];
        final int[] seed = new int[1];
        final boolean[] result = new boolean[1];
        width[0] = 25;
        height[0] = 25;
        Random rnd = new Random();
        seed[0] = rnd.nextInt();
        
        
        newMazeDialog = new Shell (mazeShell, (SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL) & ~SWT.CLOSE);
        newMazeDialog.setText("Size");
        GridLayout gl = new GridLayout(2, true);
        gl.horizontalSpacing = 20;
        gl.verticalSpacing = 20;
        gl.marginBottom = 2;
        gl.marginTop = 2;
        gl.marginLeft = 2;
        gl.marginRight = 2;
        newMazeDialog.setLayout (gl);
        
        // Width
        Label widthLabel = new Label(newMazeDialog, SWT.NONE);
        widthLabel.setText("Width");
        final Spinner spinWidth = new Spinner(newMazeDialog, SWT.NONE);
        spinWidth.setDigits(0);
        spinWidth.setIncrement(1);
        spinWidth.setMaximum(40);
        spinWidth.setMinimum(10);
        spinWidth.setSelection(width[0]);
        spinWidth.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                width[0] = spinWidth.getSelection();
            }
        });
        
        
        // Height
        Label heightLabel = new Label(newMazeDialog, SWT.NONE);
        heightLabel.setText("Height");
        final Spinner spinHeight = new Spinner(newMazeDialog, SWT.NONE);
        spinHeight.setDigits(0);
        spinHeight.setIncrement(1);
        spinHeight.setMaximum(40);
        spinHeight.setMinimum(10);
        spinHeight.setSelection(height[0]);
        spinHeight.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                height[0] = spinHeight.getSelection();
            }
        });

        // Seed
        Label seedLabel = new Label(newMazeDialog, SWT.NONE);
        seedLabel.setText("Seed");
        final Spinner spinSeed = new Spinner(newMazeDialog, SWT.NONE);
        spinSeed.setDigits(0);
        spinSeed.setIncrement(1);
        spinSeed.setMaximum(Integer.MAX_VALUE);
        spinSeed.setMinimum(Integer.MIN_VALUE);
        spinSeed.setSelection(seed[0]);
        spinSeed.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                seed[0] = spinSeed.getSelection();
            }
        });
        
        //Buttons
        final Button ok = new Button (newMazeDialog, SWT.PUSH);
        ok.setText ("OK");
        ok.addListener(SWT.Selection, new Listener() {
            @Override
            public void handleEvent(Event e) {
                result[0] = true;
                newMazeDialog.close();
            }
        });
        Button cancel = new Button (newMazeDialog, SWT.PUSH);
        cancel.setText ("Cancel");
        cancel.addListener(SWT.Selection, new Listener() {
            @Override
            public void handleEvent(Event e) {
                result[0] = false;
                newMazeDialog.close();
            }
        });
        newMazeDialog.setDefaultButton(ok);
        newMazeDialog.pack();
        newMazeDialog.open();
        
        while (!newMazeDialog.isDisposed ()) {
            if (!display.readAndDispatch ()) display.sleep ();
        }
        
        if (result[0])
        {
            Integer[] ret = new Integer[3];
            ret[0] = width[0];
            ret[1] = height[0];
            ret[2] = seed[0];
            return ret;
        }else{
            return null;            
        }
        
    }
    
    private static Maze createMaze(int width, int height, int seed)
    {
        Room entrance = new Room(0,0);
        Room exit = new Room (width - 1, height - 1);
        Maze maze = new Maze(width,height,entrance,exit, seed);
        
        return maze;
    }
    
    public static Composite createMazeComposite(Shell shell)
    {
        // Create the maze composite
        Composite composite = new Composite(shell, SWT.NONE);
        composite.setSize(shell.getClientArea().width, shell.getClientArea().height);

        
        return composite;
    }
    
    public static void showMazeWindow(Shell shell)
    {
        shell.pack();
        shell.open(); 
        Monitor primary = shell.getDisplay().getPrimaryMonitor();
        Rectangle bounds = primary.getBounds ();
        Rectangle rect = shell.getBounds ();
        int x = bounds.x + (bounds.width - rect.width) / 2;
        int y = bounds.y + (bounds.height - rect.height) / 2;
        shell.setLocation (x, y);
    }
    
    public static void main(String[] args) {

        // Create a maze with default size
        Random rnd = new Random();
        maze = createMaze(25,25,rnd.nextInt());
        
        // Create the display
        display = new Display();
        
        // Create the maze shell
        mazeShell = new Shell(display, SWT.SHELL_TRIM & ~(SWT.RESIZE | SWT.MAX));
        mazeShell.setText("Maze Server");
        try{
            InputStream is = ClassLoader.getSystemResourceAsStream("files/images/maze.gif");
            if (is == null)
            {
                is = new FileInputStream("files/images/maze.gif");
            }
            Image image = new Image(display, is);
            mazeShell.setImage(image);
        }catch(Exception e)
        {
            System.out.println("Program icon not found.");
        }
        mazeShell.setSize(400, 400);
        
        // Create the maze composite in the shell
        mazeComposite = createMazeComposite(mazeShell);

        // Create a view for the initial maze
        mazeView = new MazeView(maze, mazeComposite);
        
        // Create an agent tracker for the view
        agentTracker = new MazeAgentTracker(maze, mazeView);

        // adds a menu to the window
        addMenu(mazeShell);
        
        // Shows the maze window
        showMazeWindow(mazeShell);
        
        // Create and publish the web service
        webService = new MazeWebService (maze, display, agentTracker);
        webService.publish();
        
        // Create and check the event loop
        while (!mazeShell.isDisposed()) {
          if (!display.readAndDispatch())
              display.sleep();
        }
        display.dispose();
        
        webService.stop();
    }

    
    private static void addMenu(Shell shell)
    {
        Menu bar = new Menu (shell, SWT.BAR);
        shell.setMenuBar (bar);
        
        MenuItem fileItem = new MenuItem (bar, SWT.CASCADE);
        fileItem.setText ("&File");
        Menu submenu = new Menu (shell, SWT.DROP_DOWN);
        fileItem.setMenu (submenu);
        
        MenuItem reset = new MenuItem (submenu, SWT.PUSH);
        reset.setText ("&Reset Maze \tCtrl+R");
        reset.setAccelerator (SWT.MOD1 + 'R');
        reset.addListener (SWT.Selection, new Listener () {
            public void handleEvent (Event e) {
                agentTracker.reset();
            }
        });
        
        MenuItem newMaze = new MenuItem (submenu, SWT.PUSH);
        newMaze.setText ("&New Maze \tCtrl+N");
        newMaze.setAccelerator (SWT.MOD1 + 'N');
        newMaze.addListener (SWT.Selection, new Listener () {
            public void handleEvent (Event e) {
                Integer[] par = getNewMazeSize();
                if (par != null)
                {
                    maze = createMaze(par[0],par[1],par[2]);
                    mazeComposite.dispose();
                    mazeComposite = createMazeComposite(mazeShell);
                    mazeView = new MazeView(maze, mazeComposite);
                    agentTracker = new MazeAgentTracker(maze, mazeView);
                    showMazeWindow(mazeShell);
                    webService.stop();
                    webService = new MazeWebService (maze, display, agentTracker);
                    webService.publish();
                }
            }
        });
        
        MenuItem quit = new MenuItem (submenu, SWT.PUSH);
        quit.setText ("&Exit \tCtrl+E");
        quit.setAccelerator (SWT.MOD1 + 'E');
        quit.addListener (SWT.Selection, new Listener () {
            public void handleEvent (Event e) {
                closeServer();
            }
        });
    }

}
