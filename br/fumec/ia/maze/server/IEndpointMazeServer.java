package br.fumec.ia.maze.server;

import javax.jws.WebMethod;  
import javax.jws.WebParam;  
import javax.jws.WebResult;  
import javax.jws.WebService;  

@WebService (name = "IEndpointMazeServer", targetNamespace = "http://maze.ia.fumec.br/")
public interface IEndpointMazeServer {
	@WebMethod
	@WebResult(name="state")
	public String getState();
	
	@WebMethod
	@WebResult(name = "result")
	public String move(@WebParam (name="move") String move);

}
