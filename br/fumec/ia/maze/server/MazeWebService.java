package br.fumec.ia.maze.server;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import javax.jws.WebService; 
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.Endpoint;
import org.eclipse.swt.widgets.Display;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import br.fumec.ia.maze.Maze;
import br.fumec.ia.maze.view.MazeAgentTracker;

@WebService(targetNamespace = "http://maze.ia.fumec.br/",  
            endpointInterface = "br.fumec.ia.maze.server.IEndpointMazeServer",   
            portName = "TestMazeServerPort",   
            serviceName = "TestMazeServerService")
public class MazeWebService  implements IEndpointMazeServer, Runnable{
    private Maze maze;
    private MazeAgentTracker tracker;
    private Endpoint endpoint;
    private Maze.Movements nextMove;
    private Display display;
    
    public MazeWebService(Maze maze, Display display, MazeAgentTracker tracker)
    {
        this.maze = maze;
        this.display = display;
        this.tracker = tracker;
    }
    
    @Override
    public String getState() {
        List<Maze.Movements> moves = maze.getValidMovements(tracker.getPosition());
        
        String returnString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<maze width = \"" + maze.getWidth() +"\" " +
                    "height = \"" + maze.getHeight() + "\">" +
                    "<position x = \"" + tracker.getPosition().x + 
                    "\" y = \"" + tracker.getPosition().y + "\"/>" +
                    "<objective x = \"" + maze.getExit().x + "\" y = \"" + maze.getExit().y +"\"/>" +
                    "<moves>";
        
        if (moves.contains(Maze.Movements.RIGHT))
            returnString += "<right/>";
        
        if (moves.contains(Maze.Movements.LEFT))
            returnString += "<left/>";
        
        if (moves.contains(Maze.Movements.UP))
            returnString +=  "<up/>";
        
        if (moves.contains(Maze.Movements.DOWN))
            returnString += "<down/>";

        returnString += "</moves></maze>";
        
        return returnString;
    }

    @Override
    public String move(String move) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is = new InputSource(new StringReader(move));
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(is);
        //} catch (SAXException | IOException | ParserConfigurationException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (doc.getElementsByTagName("up").getLength() > 0)
        {
            nextMove = Maze.Movements.UP;
        }else{
            if (doc.getElementsByTagName("down").getLength() > 0)
            {
                nextMove = Maze.Movements.DOWN;
            }else{
                if (doc.getElementsByTagName("left").getLength() > 0)
                {
                    nextMove = Maze.Movements.LEFT;
                }else{

                    if (doc.getElementsByTagName("right").getLength() > 0)
                    {
                        nextMove = Maze.Movements.RIGHT;
                    }
                }
            }
        }
        
        display.syncExec(this);

        List<Maze.Movements> moves = maze.getValidMovements(tracker.getPosition());
        
        String returnString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<maze><position x = \"" + tracker.getPosition().x + 
                    "\" y = \"" + tracker.getPosition().y + "\"/><moves>";
        
        if (moves.contains(Maze.Movements.RIGHT))
            returnString += "<right/>";
        
        if (moves.contains(Maze.Movements.LEFT))
            returnString += "<left/>";
        
        if (moves.contains(Maze.Movements.UP))
            returnString +=  "<up/>";
        
        if (moves.contains(Maze.Movements.DOWN))
            returnString += "<down/>";

        returnString += "</moves></maze>";
        
        return returnString;
    }

    @Override
    public void run() {
        
        tracker.move(nextMove);
    }
    
    public void publish()
    {
        endpoint = Endpoint.publish("http://localhost:7890/endpoint", this);
    }
    
    public void stop()
    {
        if (endpoint != null)
            endpoint.stop();
    }
}
