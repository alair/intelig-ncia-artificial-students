package br.fumec.ia.maze.agents;

import br.fumec.ia.maze.Maze;
import br.fumec.ia.search.IAction;

public class MazeAction implements IAction {
    private Maze.Movements move;
    
    public MazeAction(Maze.Movements move)
    {
        this.move = move;
    }
    
    public Maze.Movements getMove()
    {
        return move;
    }
    
    @Override
    public Double getCost() {
        return 1.0;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((move == null) ? 0 : move.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MazeAction other = (MazeAction) obj;
        if (move != other.move)
            return false;
        return true;
    }

}
