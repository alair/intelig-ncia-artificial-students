package br.fumec.ia.maze.agents;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.LinkedList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import br.fumec.ia.maze.Maze;
import br.fumec.ia.maze.Room;
import br.fumec.ia.maze.client.MazeConnection;
import br.fumec.ia.search.IAction;
import br.fumec.ia.search.IState;
import br.fumec.ia.search.SearchProblem;

public class MazeProblem implements SearchProblem {

    private IState exit;
    private IState entrance;
    private IState current;
    private MazeConnection mazeConnection;
    private HashMap<IState, Node> backtrackPath;
    private String endpoint;
    
    private class Node
    {
        public IState current;
        public Node parent;
        public Maze.Movements move;
        public int depth;
        public LinkedList<IAction> moves;

        
        public Node(IState current, Node parent,
        		Maze.Movements move, int depth, LinkedList<IAction> moves)
        {
            this.current = current;
            this.parent = parent;
            this.move = move;
            this.depth = depth;
            this.moves = moves;
        }
        
    }
    
    public MazeProblem(String endpoint)
    {
        this.mazeConnection = new MazeConnection();
        backtrackPath = new HashMap<IState, Node>();
        this.endpoint = endpoint;

        String result = "";
        try{
            result = mazeConnection.getState(this.endpoint);
        }catch(Exception e){
            e.printStackTrace();
        }
        this.exit = ((MazeState)getExitFromString(result));
        MazeState entrance = (MazeState)getStateFromString(result);
        this.entrance = entrance;
        this.current = entrance;
        backtrackPath.put(entrance, new Node(entrance, null, null
        		, 0, getActionsFromString(result)));
    }
    
    @Override
    public boolean isObjective(IState state) throws ClassCastException {
        return ((MazeState)state).equals(exit);
    }

    @Override
    public IState getInitialState() throws ClassCastException {
        return entrance;
    }
    
    public IState getExit()
    {
        return exit;
    }

    private LinkedList<IAction> getActionsFromString(String movements)
    {
        LinkedList<IAction> moves = new LinkedList<IAction>();
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is = new InputSource(new StringReader(movements));
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(is);
        //} catch (SAXException | IOException | ParserConfigurationException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeList nodes = doc.getElementsByTagName("moves");
        if (nodes.getLength() > 0)
        {
            NodeList movesNodes = nodes.item(0).getChildNodes();
            for (int i = 0; i < movesNodes.getLength(); ++i)
            {
                if (movesNodes.item(i).getNodeName() == "up")
                {
                    moves.add(new MazeAction(Maze.Movements.UP));
                }else{
                    if (movesNodes.item(i).getNodeName() == "down")
                    {
                        moves.add(new MazeAction(Maze.Movements.DOWN));
                    }else{
                        if (movesNodes.item(i).getNodeName() == "left")
                        {
                            moves.add(new MazeAction(Maze.Movements.LEFT));
                        }else{
                            if (movesNodes.item(i).getNodeName() == "right")
                            {
                                moves.add(new MazeAction(Maze.Movements.RIGHT));
                            }
                        }
                    }
                }
            }       
        }
        
        return moves;
    }
    
    private IState getExitFromString(String movements)
    {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is = new InputSource(new StringReader(movements));
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(is);
        //} catch (SAXException | IOException | ParserConfigurationException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeList nodes = doc.getElementsByTagName("objective");
        
        Integer x = 0;
        Integer y = 0;
        
        if (nodes.getLength() > 0)
        {
            x = Integer.valueOf(nodes.item(0).getAttributes().getNamedItem("x").getNodeValue());
            y = Integer.valueOf(nodes.item(0).getAttributes().getNamedItem("y").getNodeValue());
        }
        
        Room room = new Room(x,y);
        return new MazeState(room, 0);
    }
    
    private IState getStateFromString(String movements)
    {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is = new InputSource(new StringReader(movements));
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(is);
        //} catch (SAXException | IOException | ParserConfigurationException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeList nodes = doc.getElementsByTagName("position");
        
        Integer x = 0;
        Integer y = 0;
        
        if (nodes.getLength() > 0)
        {
            x = Integer.valueOf(nodes.item(0).getAttributes().getNamedItem("x").getNodeValue());
            y = Integer.valueOf(nodes.item(0).getAttributes().getNamedItem("y").getNodeValue());
        }
        
        Room room = new Room(x,y);
        return new MazeState(room, ((MazeState)this.exit).getRoom().distance(room));
    }
    
    public String goToState(IState state)
    {
        
        java.util.Stack<Maze.Movements> stack = new java.util.Stack<Maze.Movements>();
        
        Node currentNode = backtrackPath.get(this.current);
        Node destNode = backtrackPath.get(state);
        
        while(!currentNode.current.equals(destNode.current))
        {
            if (currentNode.depth > destNode.depth)
            {
                try {
                    switch(currentNode.move)
                    {
                    case UP:
                        mazeConnection.move(this.endpoint, Maze.Movements.DOWN);
                        break;
                    case DOWN:
                        mazeConnection.move(this.endpoint, Maze.Movements.UP);
                        break;
                    case LEFT:
                        mazeConnection.move(this.endpoint, Maze.Movements.RIGHT);
                        break;
                    case RIGHT:
                        mazeConnection.move(this.endpoint, Maze.Movements.LEFT);
                        break;
                    }
                    
                //} catch (MalformedURLException | SOAPException e) {
                } catch (Exception e) {
                    e.printStackTrace();
                }
                currentNode = currentNode.parent;
                this.current = currentNode.current;
            }else{
                stack.add(destNode.move);
                destNode = destNode.parent;
            }
        }
        
        String position = null;
        
        while(!stack.isEmpty()){
            try {
                position = mazeConnection.move(this.endpoint, stack.pop());
            //} catch (MalformedURLException | SOAPException e) {
            } catch (Exception e) {
                e.printStackTrace();
            } 
        }
        
        this.current = state; //= getStateFromString(position);
        
        if (backtrackPath.get(state).moves == null)
        {
        	backtrackPath.get(state).moves = getActionsFromString(position);
        }
        
        return position;
    }
    
    @Override
    public LinkedList<IAction> getValidActions(IState state)
            throws ClassCastException {
        
        String movements = "";
        
        if (this.current.equals(state))
        {
            try{
                movements = mazeConnection.getState(this.endpoint);
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            movements = goToState(state);
        }
        
        LinkedList<IAction> moves = null;
        
        if (backtrackPath.containsKey(state))
        {
        	moves = backtrackPath.get(state).moves;
        }else{
        	moves = getActionsFromString(movements);
        }
        
        for (IAction a : moves)
        {
            MazeAction ma = (MazeAction)a;
            MazeState thisState = (MazeState)execute(state, a);
            if (!backtrackPath.containsKey(thisState))
            {
            	Node parent = backtrackPath.get(state);
            	Node thisNode = new Node(thisState, parent, 
            			ma.getMove(), parent.depth + 1, null);
            	backtrackPath.put(thisState, thisNode);
            }
            	
        }
        
        return moves;
        
    }

    @Override
    public IState execute(IState state, IAction action) {
        
        // This is a false execution
        
        Integer x = ((MazeState)state).getRoom().x;
        Integer y = ((MazeState)state).getRoom().y;
        
        switch (((MazeAction)action).getMove())
        {
        case UP:
            --y;
            break;
        case DOWN:
            ++y;
            break;
        case LEFT:
            --x;
            break;
        case RIGHT:
            ++x;
            break;
        }
        Room room = new Room(x,y);
        return new MazeState(room, ((MazeState)this.exit).getRoom().distance(room));
    }

}
