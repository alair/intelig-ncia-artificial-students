package br.fumec.ia.maze.agents;

import br.fumec.ia.maze.Room;
import br.fumec.ia.search.IState;

public class MazeState implements IState {
    private Room room;
    private Double evaluation; 
    
    public MazeState(Room state, Integer eval)
    {
        this.room = state;
        evaluation = new Double(eval);
    }
    
    public void setEval(Double eval)
    {
        evaluation = eval;
    }
    
    public Room getRoom()
    {
        return room;
    }
    
    @Override
    public Double eval() {
        return evaluation;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((room == null) ? 0 : room.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MazeState other = (MazeState) obj;
        if (room == null) {
            if (other.room != null)
                return false;
        } else if (!room.equals(other.room))
            return false;
        return true;
    }

}
