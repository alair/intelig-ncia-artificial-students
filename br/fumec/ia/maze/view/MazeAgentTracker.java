package br.fumec.ia.maze.view;

import org.eclipse.swt.SWT;

import br.fumec.ia.maze.Maze;
import br.fumec.ia.maze.Room;

public class MazeAgentTracker {

    public enum RoomState
    {
        UNVISITED,
        VISITED,
        SOLUTION_PATH,
        ENTRANCE,
        EXIT
    }
    
    private Maze maze;
    private MazeView view;
    private RoomState roomState[][];
    private Room position;
    private boolean firstMove = true;
    private boolean solution = false;
    private long moves = 0;
    private long time = 0;
    
    public void reset()
    {
        firstMove = true;
        solution = false;
        time = System.currentTimeMillis();
        this.position = maze.getEntrance();
        
        for (int i = 0; i < maze.getWidth(); ++i)
        {
            for(int j = 0; j < maze.getHeight(); ++j)
            {
                if (i == maze.getEntrance().x && j == maze.getEntrance().y)
                {
                    this.roomState[i][j] = RoomState.ENTRANCE;
                    view.setRoomColor(i, j, SWT.COLOR_YELLOW);
                }else{
                    if (i == maze.getExit().x && j == maze.getExit().y)
                    {
                        this.roomState[i][j] = RoomState.EXIT;
                        view.setRoomColor(i, j, SWT.COLOR_RED);
                    }else{
                        this.roomState[i][j] = RoomState.UNVISITED;
                        view.setRoomColor(i, j, SWT.COLOR_WHITE);
                    }
                }
            }
        }
       
    }
    
    public MazeAgentTracker(Maze maze, MazeView view)
    {
        this.maze = maze;
        this.view = view;
        this.roomState = new RoomState[maze.getWidth()][maze.getHeight()];
        
        reset();
    }
    
    public void move(Maze.Movements m)
    {
        if (solution)
            return;
        
        if (firstMove)
        {
            moves = 0;
            solution = false;
            time = System.currentTimeMillis();
        }
        ++moves;
        firstMove = false;
        if (this.maze.getValidMovements(this.position).contains(m))
        {
            Integer toRoomX = new Integer(this.position.x);
            Integer toRoomY = new Integer(this.position.y);
            
            switch(m)
            {
            case UP:
                toRoomY = this.position.y-1;
                break;
                
            case DOWN:
                toRoomY = this.position.y+1;
                break;
                
            case LEFT:
                toRoomX = this.position.x-1;
                break;
                
            case RIGHT:
                toRoomX = this.position.x+1;
                break;
            }
            
            if (this.roomState[toRoomX][toRoomY] == RoomState.UNVISITED ||
                    this.roomState[toRoomX][toRoomY] == RoomState.VISITED)
            {
                this.roomState[toRoomX][toRoomY] = RoomState.SOLUTION_PATH;
                this.view.setRoomColor(toRoomX, toRoomY, SWT.COLOR_YELLOW);
            }else{
                if (this.roomState[toRoomX][toRoomY] == RoomState.SOLUTION_PATH)
                {
                    this.roomState[toRoomX][toRoomY] = RoomState.SOLUTION_PATH;
                    this.view.setRoomColor(toRoomX, toRoomY, SWT.COLOR_YELLOW);
                    this.roomState[this.position.x][this.position.y] = RoomState.VISITED;
                    this.view.setRoomColor(this.position.x, this.position.y, SWT.COLOR_GRAY);
                }else{
                    if (this.roomState[toRoomX][toRoomY] == RoomState.ENTRANCE)
                    {
                        this.roomState[this.position.x][this.position.y] = RoomState.VISITED;
                        this.view.setRoomColor(this.position.x, this.position.y, SWT.COLOR_GRAY);
                    }
                }
            }
            this.position.x = toRoomX;
            this.position.y = toRoomY;
            if (position.equals(maze.getExit()))
            {
                solution = true;
                time = System.currentTimeMillis() - time;
                System.out.println("Solved in: " + time + " ms with " + moves + " moves.");
            }
        }
    }
    
    public Room getPosition()
    {
        return this.position;
    }
    
    public long getMoves()
    {
        return moves;
    }
    
    public long getTime()
    {
        if (solution)
            return time;
        else
            return System.currentTimeMillis() - time;
    }
    
    public boolean solved()
    {
        return solution;
    }
}
