package br.fumec.ia.maze.view;

import java.util.List;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import br.fumec.ia.maze.Maze;
import br.fumec.ia.maze.Room;


public class MazeView {
    private Maze maze;
    private Composite composite;
    private Canvas canvas[][];
    
    public MazeView (Maze maze, Composite composite)
    {
        this.maze = maze;
        this.composite = composite;
        
        drawMaze();
    }
 
    public void redrawMaze()
    {
     // Create a grid to place the Rooms
        GridLayout gl = new GridLayout(this.maze.getWidth(), true);
        gl.horizontalSpacing = 0;
        gl.verticalSpacing = 0;
        gl.marginBottom = 2;
        gl.marginTop = 2;
        gl.marginLeft = 2;
        gl.marginRight = 2;
        this.composite.setLayout (gl);
        
        // Calculate the size of the cell
        Integer cellWidth = composite.getClientArea().width / maze.getWidth();
        Integer cellHeight = composite.getClientArea().height / maze.getHeight();;
        
        // Set the size of the grid
        GridData gridData = new GridData();
        gridData.widthHint = cellWidth;
        gridData.heightHint = cellHeight;

        // Set the composite background
        this.composite.setBackground(composite.getDisplay().getSystemColor (SWT.COLOR_BLACK));
        
        for (int j = 0; j < this.maze.getHeight(); ++j)
        {
            for (int i = 0; i < this.maze.getWidth(); ++i)
            {
                this.canvas[i][j].setLayoutData(gridData);                
            }
        }
        this.composite.pack();
        composite.layout();
        
    }
    
    private void drawMaze()
    {
        // create one canvas for each room 
        this.canvas = new Canvas[this.maze.getWidth()][this.maze.getHeight()]; 
        
        // Create a grid to place the Rooms
        GridLayout gl = new GridLayout(this.maze.getWidth(), true);
        gl.horizontalSpacing = 0;
        gl.verticalSpacing = 0;
        gl.marginBottom = 2;
        gl.marginTop = 2;
        gl.marginLeft = 2;
        gl.marginRight = 2;
        this.composite.setLayout (gl);
        
        // Calculate the size of the cell
        Integer cellWidth = composite.getClientArea().width / maze.getWidth();
        Integer cellHeight = composite.getClientArea().height / maze.getHeight();;
        
        // Set the size of the grid
        GridData gridData = new GridData();
        gridData.widthHint = cellWidth;
        gridData.heightHint = cellHeight;

        // Set the composite background
        this.composite.setBackground(composite.getDisplay().getSystemColor (SWT.COLOR_BLACK));
        
        for (int j = 0; j < this.maze.getHeight(); ++j)
        {
            for (int i = 0; i < this.maze.getWidth(); ++i)
            {
                this.canvas[i][j] = new Canvas(this.composite, SWT.NONE);
                this.canvas[i][j].setLayoutData(gridData);
                
                // create the walls of the room
                int walls = 0;
                List<Maze.Movements> moves = maze.getValidMovements(new Room(i,j));
                if (!moves.contains(Maze.Movements.UP) && j > 0)
                    walls = walls | MazeWallListener.TOP;
                
                if (!moves.contains(Maze.Movements.DOWN) && j < this.maze.getHeight() - 1)
                    walls = walls | MazeWallListener.BOTTOM;
                
                if (!moves.contains(Maze.Movements.LEFT) && i > 0)
                    walls = walls | MazeWallListener.LEFT;
                
                if (!moves.contains(Maze.Movements.RIGHT) && i < this.maze.getWidth() - 1)
                    walls = walls | MazeWallListener.RIGHT;
                
                this.canvas[i][j].addPaintListener(new MazeWallListener(walls));
            }
        }
        this.composite.pack();
        composite.layout();
    }

    public void setRoomColor(Integer x, Integer y, int color)
    {
        this.canvas[x][y].setBackground(composite.getDisplay().getSystemColor(color));
    }
}

class MazeWallListener implements PaintListener
{
    public static final int TOP = 1;
    public static final int BOTTOM = 2;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    
    private int walls;
    
    public MazeWallListener(int walls)
    {
        this.walls = walls;
    }

    @Override
    public void paintControl(PaintEvent e) {
        // Get the canvas and its size
        Canvas canvas = (Canvas) e.widget;
        int maxX = canvas.getSize().x-1;
        int maxY = canvas.getSize().y-1;
        
        // Set the drawing color to blue
        e.gc.setForeground(e.display.getSystemColor(SWT.COLOR_BLACK));
        
        // Set the width of the lines to draw
        e.gc.setLineWidth(1);
        
        // top
        if ((this.walls & TOP) == TOP)
            e.gc.drawLine(0, 0, maxX, 0);
        // bottom
        if ((this.walls & BOTTOM) == BOTTOM)
            e.gc.drawLine(0, maxY, maxX, maxY);
        // left
        if ((this.walls & LEFT) == LEFT)
            e.gc.drawLine(0, 0, 0, maxY);
        // right
        if ((this.walls & RIGHT) == RIGHT)
            e.gc.drawLine(maxX, 0, maxX, maxY);
    }
    
}