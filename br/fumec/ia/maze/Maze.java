package br.fumec.ia.maze;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.jgrapht.alg.KruskalMinimumSpanningTree;
import org.jgrapht.graph.*;

public class Maze {
	
    private SimpleWeightedGraph<Room, DefaultWeightedEdge> maze = 
            new SimpleWeightedGraph<Room,DefaultWeightedEdge>(DefaultWeightedEdge.class);
    private Integer width = new Integer(0);
    private Integer height = new Integer(0);
    private Room entrance;
    private Room exit;
    
    
    public enum Movements
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
    
    
    public Maze(Integer width, Integer height, Room entrance, Room exit)
    {
        createMaze(width, height, entrance, exit, null);
    }
    
    public Maze(Integer width, Integer height, Room entrance, Room exit, Integer seed)
    {
        createMaze(width, height, entrance, exit, seed);
    }
    
    private void createMaze(Integer width, Integer height, Room entrance, Room exit, Integer seed)
    {
        this.width = width;
        this.height = height;
        this.entrance = entrance;
        this.exit = exit;
        
        Random rnd = (seed == null ? new Random() : new Random(seed));
        
        for (int i = 0; i < this.width; ++i)
        {
            for (int j = 0; j < this.height; ++j)
            {
                maze.addVertex(new Room(i,j));
                if (i > 0)
                {
                    maze.setEdgeWeight(maze.addEdge(new Room(i-1,j), new Room(i,j)),rnd.nextDouble());
                }
                
                if (j > 0)
                    maze.setEdgeWeight(maze.addEdge(new Room(i,j-1), new Room(i,j)),rnd.nextDouble());
            }
        }
        
        KruskalMinimumSpanningTree<Room,DefaultWeightedEdge> k = new KruskalMinimumSpanningTree<Room,DefaultWeightedEdge>(maze);
        
        @SuppressWarnings("unchecked")
        SimpleWeightedGraph<Room, DefaultWeightedEdge> clone = (SimpleWeightedGraph<Room, DefaultWeightedEdge>)maze.clone();
        
        clone.removeAllEdges(k.getEdgeSet());
        maze.removeAllEdges(clone.edgeSet());        
    }
    
    public List<Movements> getValidMovements(Room room)
    {
        LinkedList<Movements> moves = new LinkedList<Movements>();
        
        for (DefaultWeightedEdge e : maze.edgesOf(room))
        {   
            Room target = maze.getEdgeTarget(e);
            Room source = maze.getEdgeSource(e);
            
            // up
            if (target.y == room.y - 1 || source.y == room.y - 1)
            {
                moves.add(Movements.UP);
            }

            // down
            if (target.y == room.y + 1 || source.y == room.y + 1)
            {
                moves.add(Movements.DOWN);
            }
            
            // left
            if (target.x == room.x - 1 || source.x == room.x - 1)
            {
                moves.add(Movements.LEFT);
            }

            // left
            if (target.x == room.x + 1 || source.x == room.x + 1)
            {
                moves.add(Movements.RIGHT);
            }
        }
        
        return moves;
    }
    
    public Room getEntrance()
    {
        return new Room(this.entrance.x, this.entrance.y);
    }
    
    public Room getExit()
    {
        return new Room(this.exit.x, this.exit.y);
    }
    
    public Integer getWidth()
    {
        return new Integer(this.width);
    }

    public Integer getHeight()
    {
        return new Integer(this.height);
    }
}
