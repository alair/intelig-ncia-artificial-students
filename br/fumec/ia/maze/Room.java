package br.fumec.ia.maze;

public class Room {
    public Integer x;
    public Integer y;
    
    public Room(Integer x, Integer y)
    {
        this.x = x;
        this.y = y;
    }
    
    public Integer distance(Room room)
    {
        //return (int)Math.sqrt(Math.pow(new Double(Math.abs(room.x - this.x)),2.0) + 
        //        Math.pow(new Double(Math.abs(room.y - this.y)), 2.0));
        return Math.abs(room.x - this.x) + Math.abs(room.y - this.y);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((x == null) ? 0 : x.hashCode());
        result = prime * result + ((y == null) ? 0 : y.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Room other = (Room) obj;
        if (x == null) {
            if (other.x != null)
                return false;
        } else if (!x.equals(other.x))
            return false;
        if (y == null) {
            if (other.y != null)
                return false;
        } else if (!y.equals(other.y))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
    
    
}
